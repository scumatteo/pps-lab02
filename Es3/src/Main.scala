object Main extends App{

  val parity: Int => String = {
    case v if (v % 2 == 0) => "Even"
    case _ => "Odd"
  }

  def parityMethod(v: Int): String = v match {
    case v if v % 2 == 0 => "Even"
    case _ => "Odd"
  }

  val neg: (String => Boolean) => (String => Boolean) = f => s => !f(s)

  def negMethod(f : String =>  Boolean) : (String => Boolean) = !f(_)

  val empty: String => Boolean = _==""
  val notEmpty = neg(empty)

  val notEmptyMethod = negMethod(empty)
  println(parity(11))
  println(parityMethod(10))

  println(notEmpty(""))
  println(notEmpty("foo"))
  println(notEmpty("foo") && !notEmpty(""))

  println(notEmptyMethod(""))
  println(notEmptyMethod("foo"))
  println(notEmptyMethod("foo") && !notEmpty(""))

}
